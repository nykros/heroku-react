# React Static build en Heroku comunicándose con otro Heroku con Java

## Román García

El proyecto usa dos proxys:

- En desarrollo, el que esta especificado en el `package.json`

```
  "proxy": "http://localhost:8080",
```

- Cuando está deployado en Heroku, el definido en `static.json` y la variable de ambiente: API_URL

```
{
  "root": "build/",
  "routes": {
    "/**": "index.html"
  },
  "proxies": {
    "/api/": {
      "origin": "${API_URL}/api/"
    }
  }
}
```

Leer: https://github.com/mars/create-react-app-buildpack
