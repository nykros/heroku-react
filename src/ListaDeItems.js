import React from 'react';
import axios from 'axios';

export default class ListaDeItems extends React.Component {
  constructor(props) {
    super(props);
    this.state = { items: [] };

    getItems()
      .then(items => {
        console.table(items);
        this.setState({ items });
      })
      .catch(err => console.error(err));
  }

  render() {
    return (
      <div>
        <h2>Cosas para hacer</h2>
        <ul>
          {this.state.items.map(item => (
            <li>
              {item.hora}&nbsp;
              {item.descripcion}
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

// ----------------------- Funciones auxiliares -----------------------

// Uso path relativo porque uso el proxy indicado en el package.json
const getItems = () => {
  return getData(axios.get('/api/items'));
};

const getData = axiosPromise => {
  return new Promise((resolve, reject) =>
    axiosPromise.then(res => resolve(res.data)).catch(err => reject(err))
  );
};
