import React from 'react';
import './App.css';
import ListaDeItems from './ListaDeItems';

function App() {
  return (
    <div className="App">
      <ListaDeItems></ListaDeItems>
    </div>
  );
}

export default App;
